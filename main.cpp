#include <QApplication>
#include <QCommandLineParser>
#include "mainwindow.h"

// ./clock --font "Steve" --pointSize 72 --format "hh:mm:ss.zzz"

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    QApplication::setApplicationName("Clock");
    QApplication::setApplicationDisplayName("Clock");

    QCommandLineParser parser;
    parser.addOptions({
                              {"font",      "Font Family", "font"},
                              {"pointSize", "PointSize",   "pointSize"},
                              {"format",    "format",      "format"}});

    parser.process(app);

    QString family = "Ariel";
    int pointSize = 36;
    QString format = "dd/MM/yyyy hh:mm:ss.zzz";

    if (!parser.value("font").isEmpty())
        family = parser.value("font");

    if (!parser.value("pointSize").isEmpty())
        pointSize = parser.value("pointSize").toInt();

    if (!parser.value("format").isEmpty())
        format = parser.value("format");

    MainWindow w(nullptr, family, pointSize, format);
    w.show();

    return QApplication::exec();
}
