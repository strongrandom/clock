# clock

Simple clock using Qt. Attempts to be accurate to the millisecond.

# Building

```
$ mkdir build && cd build && cmake ..
$ make
```

It also builds in CLion and should build in Qt Creator. 
