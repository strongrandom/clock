#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr, const QString &family = "Ariel", int pointSize = 36,
                        QString format = "dd/MM/yyyy hh:mm:ss.zzz");

    ~MainWindow() override;

private:
    Ui::MainWindow *ui;
    QString format;

private slots:

    void on_update_time();
};

#endif // MAINWINDOW_H
