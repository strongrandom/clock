#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDateTime>
#include <QTimer>
#include <QThread>
#include <utility>


MainWindow::MainWindow(QWidget *parent, const QString &family, int pointSize, QString format) :
        QMainWindow(parent),
        ui(new Ui::MainWindow) {
    ui->setupUi(this);

    QFont f(family, pointSize, QFont::Normal);
    ui->labelTime->setFont(f);
    this->format = std::move(format);

    QDateTime now = QDateTime::currentDateTime();
    QTimer::singleShot((1000 - now.time().msec()), this, SLOT(on_update_time()));
    ui->labelTime->setText(now.toString(this->format));
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_update_time() {
    QDateTime now = QDateTime::currentDateTime();

    while (now.time().msec() != 0) {
        QThread::usleep(100);
        now = QDateTime::currentDateTime();
    }

    ui->labelTime->setText(now.toString(this->format));

    QTimer::singleShot((995 - now.time().msec()), this, SLOT(on_update_time()));
}
